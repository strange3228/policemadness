Public Class Form1
    Dim speed As Integer
    Dim speedTracking As Integer
    Dim road(7) As PictureBox
    Dim score As Integer
    Dim f As Integer = 0

Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        road(0) = PictureBox1

        road(1) = PictureBox2

        road(2) = PictureBox3

        road(3) = PictureBox4

        road(4) = PictureBox5

        road(5) = PictureBox6

        road(6) = PictureBox7

        road(7) = PictureBox8

    End Sub

Private Sub RoadMover_Tick(sender As Object, e As EventArgs) Handles RoadMover.Tick

        For x As Integer = 0 To 7

            road(x).Left -= speed

    End Sub
    
    Public Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        f = 1
        speed = TrackBar1.Value
        RoadMover.Enabled = True
        Enemy1Move.Enabled = True
        Enemy2Move.Enabled = True
        Enemy3Move.Enabled = True
        Button3.Visible = False
        CAR.Visible = True
        Enemy1.Visible = True
        Enemy2.Visible = True
        Enemy3.Visible = True
        Label1.Visible = False
        Label2.Visible = False
        TrackBar1.Visible = False
        speedTracking = TrackBar1.Value
        Label3.Visible = False
        Label4.Visible = False
        Label5.Visible = False
    End Sub
    
    Private Sub Form1_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
	        If e.KeyCode = Keys.S Then
	            DownMove.Start()
	        End If
	        If e.KeyCode = Keys.W Then
	            UpMove.Start()
	        End If
	    End Sub
    
    Public Sub RoadMover_Tick(sender As Object, e As EventArgs) Handles RoadMover.Tick
        For x As Integer = 0 To 7
            road(x).Left -= speed

            If road(x).Left <= -135 Then
                road(x).Left = 600
            End If
        Next

        If score > 10 And score < 20 Then
            speed = TrackBar1.Value + 1
        End If

        If score > 20 And score < 30 Then
            speed = TrackBar1.Value + 2
        End If

        If score > 30 And score < 40 Then
            speed = TrackBar1.Value + 3
        End If

        If score > 40 And score < 50 Then
            speed = TrackBar1.Value + 4
        End If

        If score > 50 And score < 60 Then
            speed = TrackBar1.Value + 5
        End If
        
        SpeedText.Text = "Speed " & speed
        
         If (CAR.Bounds.IntersectsWith(Enemy1.Bounds)) Then
            gameOver()
        End If
        If (CAR.Bounds.IntersectsWith(Enemy2.Bounds)) Then
            gameOver()
        End If
        If (CAR.Bounds.IntersectsWith(Enemy3.Bounds)) Then
            gameOver()
        End If
End Sub

Public Sub gameOver()
        GameOverText.Visible = True
        RoadMover.Stop()
        Enemy1Move.Stop()
        Enemy2Move.Stop()
        Enemy3Move.Stop()
        Button1.Visible = True
        Button2.Visible = True
        TrackBar1.Visible = True
        Label3.Visible = True
        Label4.Visible = True
        Label5.Visible = True
    End Sub
Public Sub DownMove_Tick(sender As Object, e As EventArgs) Handles DownMove.Tick
        If CAR.Top < 220 Then
            CAR.Top += 5
        End If
    End Sub
Public Sub UpMove_Tick(sender As Object, e As EventArgs) Handles UpMove.Tick
        If CAR.Top > 5 Then
            CAR.Top -= 5
        End If
    End Sub
Private Sub Form1_KeyUp(sender As Object, e As KeyEventArgs) Handles MyBase.KeyUp
	        UpMove.Stop()
	        DownMove.Stop()
	    End Sub
        
Private Sub Enemy1Move_Tick(sender As Object, e As EventArgs) Handles Enemy1Move.Tick
	        Enemy1.Left -= speed + 1
	
	        If Enemy1.Left <= -94 Then
	            score += 1
	            ScoreText.Text = "Score " & score
	
	            Enemy1.Left = 600
	            Enemy1.Top = CInt(Math.Ceiling(Rnd() * 61)) + 0
	        End If
	    End Sub

Private Sub Enemy2Move_Tick(sender As Object, e As EventArgs) Handles Enemy2Move.Tick
	        Enemy2.Left -= speed + 3
	
	        If Enemy2.Left <= -97 Then
	            score += 1
	            ScoreText.Text = "Score " & score
	
	            Enemy2.Left = 600
	            Enemy2.Top = CInt(Math.Ceiling(Rnd() * 61)) + 100
	        End If
	    End Sub

Private Sub Enemy3Move_Tick(sender As Object, e As EventArgs) Handles Enemy3Move.Tick
	        Enemy3.Left -= speed + 2
	
	        If Enemy3.Left <= -85 Then
	            score += 1
	            ScoreText.Text = "Score " & score
	
	            Enemy3.Left = 600
	            Enemy3.Top = CInt(Math.Ceiling(Rnd() * 30)) + 200
	        End If
	    End Sub
        
Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
	        Me.Close()
	    End Sub
        
Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
	        score = 0
	        speedTracking = TrackBar1.Value
	        Me.Controls.Clear()
	        InitializeComponent()
	        Form1_Load(e, e)
	    End Sub
End Class